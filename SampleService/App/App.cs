﻿using SampleService.Service;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace SampleService.App
{
    class App
    {
        static void Main(string[] args)
        {
            var httpAddr = new Uri("http://localhost:8080/sample/wcf");
            var msmqAddr = new Uri("net.msmq://localhost/private/msmq");
            var msmqMexAddr = new Uri("http://localhost:8081/msmq");
            var msmqAddrs = new Uri[2];
            msmqAddrs[0] = msmqAddr;
            msmqAddrs[1] = msmqMexAddr;
            using(var httpHost = new ServiceHost(typeof(WCFService), httpAddr))
            using (var msmqHost = new ServiceHost(typeof(MSMQService), msmqAddrs))
            {
                httpHost.Open();
                msmqHost.Open();

                Console.WriteLine("Press <enter> to quit");
                Console.ReadLine();
            }
        }
    }
}
