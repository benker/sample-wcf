﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleService.Service
{
    class MSMQService : IMSMQService
    {
        void IMSMQService.HandleRequest(string request)
        {
            Console.WriteLine("got request {0}", request);
        }
    }
}
