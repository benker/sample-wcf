﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleService.Service
{
    class WCFService : IWCFService
    {
        string IWCFService.GetTime(bool UTC)
        {
            if (UTC)
            {
                return DateTime.UtcNow.ToString();
            }
            return DateTime.Now.ToString();
        }
    }
}
