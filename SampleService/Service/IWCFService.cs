﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace SampleService.Service
{
    [ServiceContract]
    public interface IWCFService
    {
        [OperationContract]
        string GetTime(bool UTC);
    }
}
