﻿using System.Diagnostics;
using System.ServiceModel;

namespace MSMQClient
{

    [ServiceContract]
    public interface IMSMQService
    {
        [OperationContract(IsOneWay = true)]
        void HandleRequest(string request);
    }

    public class Client<T> : ClientBase<T> where T: class
    {
        public T Instance
        {
            get
            {
                return Channel;
            }
        }
    }

    class Client
    {
        static void Main(string[] args)
        {
            using (var client = new Client<IMSMQService>())
            {
                client.Instance.HandleRequest("Hello Kitty!!");
            }
            
        }
    }
}
